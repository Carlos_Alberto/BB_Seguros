var mongoClient = require("mongodb").MongoClient;

mongoClient.connect("mongodb://localhost/dbnode")
            .then(conn => global.conn = conn.db("dbnode"))
            .catch(err => console.log(err))

/* funcao para pesquisar todos os clientes */
function findAll(callback){  
    global.conn.collection("cliente").find({}).toArray(callback);
}    

/* funcao para inserir dados na tabela de cliente */
function insert(cliente, callback){  
    global.conn.collection("cliente").insert(cliente, callback);
};

/* funcao para editar um registro */
var ObjectId = require("mongodb").ObjectId;
function findOne(id, callback){
    global.conn.collection("cliente").find(new ObjectId(id)).toArray(callback);
};

/* funcao para atualizar um registro */
function update(id, cliente, callback){
    global.conn.collection("cliente").update({_id:new ObjectId(id)}, cliente, callback);
};

/* funcao para deletar um registro */
function deleteOne(id, callback){
    global.conn.collection("cliente").deleteOne({_id:new ObjectId(id)},callback);
}

module.exports = {findAll,insert, findOne, update, deleteOne}